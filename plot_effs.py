import ROOT as rt
import sys



if __name__ == "__main__":
    if len(sys.argv) < 2:
        print 'usage: python plots_danek.py [clus_ana.root]'
        exit(1)
    inputfiles = sys.argv[1:]
    file_descriptions = {inputfile: inputfile[inputfile.index('clus_ana') + 8: inputfile.index('.root')].strip('_') for inputfile in inputfiles}

    # Get CMSSW area, at least CMSSW_9_2_0_patch2.
    # Get the code from git:
    #     git clone git@github.com:cms-analysis/DPGAnalysis-SiPixelTools.git DPGAnalysis-SiPixelTools
    # 
    #     Go to
    #     DPGAnalysis-SiPixelTools/HitAnalyzer/test
    #     and do:  scram b

    # See also 
    # ~dkotlins/public/CMSSW/CMSSW_9_2_0_patch2/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test

    # Scripts:
    # There are scripts to run individual  test:
    #     PixClusterAna.py - to look at all clusters
    #     testTracks.py - to look at clusters on track
    #     etc.
    #     For the present analysis I use: PixClusterAndTrack.py
    #     which combines all and on-track clusters.

    # cmsRun PixClusterAndTrack.py
    # gives clus_ana.root
    # clus_ana.root has 4 folders:
    #     (d) - all clusters
    #     (c) - clusters on tracks
    #     (c2) - clusters on tracks for tracks close to 0
    #     (c1) - clusters close to eta-1.8

    # (c) - clusters on tracks
    clusters_on_tracks = 'c'
    dirs = {'c': 'clusters on tracks', 'd': 'all clusters', 'c1': 'clusters on tracks for tracks close to \eta=0', 'c2': 'clusters close to \eta=1.8'}
    rootfile = rt.TFile(inputfile)

    # hEtaN & hPhiN - eta/phi distribution of tracks which have a hit in layer N
    track_eta_dist_geq_one_bpix_hit_l1 = "hEta1"

    # hEtaP - eta distribution of track which have at least 1 bpix hit
    track_eta_dist_geq_one_bpix_hit_overall = "hEtaP"
    # Get l1 efficiency in tracking:
    directory = 'c'
    obj = directory + '/' + track_eta_dist_geq_one_bpix_hit_l1
    print "retrieving " + str(obj) + "..."
    l1 = rootfile.Get(obj)
    l1.Draw()
    print "l1bins:", l1.GetNbinsX()
    # rt.gPad.SaveAs("track_eta_dist_geq_one_bpix_hit_l1.png")
    obj = directory + '/' + track_eta_dist_geq_one_bpix_hit_overall
    print "retrieving " + str(obj) + "..."
    overall = rootfile.Get(obj)
    print "overall bins:", overall.GetNbinsX()
    overall.Rebin(overall.GetNbinsX()/l1.GetNbinsX())
    print "overall bins:", overall.GetNbinsX()
    l1.Divide(overall)
    runs = file_description.replace('_', ' ')
    l1.SetTitle(runs + ' --- ' + dirs[directory])
    l1.Draw()
    rt.gPad.SaveAs(file_description + "_track_eta_dist_geq_one_bpix_hit_l1eff_" + directory + ".png")
    rt.gPad.SaveAs(file_description + "_track_eta_dist_geq_one_bpix_hit_l1eff_" + directory + ".pdf")


