import ROOT as rt
import sys
import matplotlib as mpl
mpl.use('Agg')
import numpy as np

import matplotlib.pyplot as plt



def get_onedim_hist_entries(onedimhist):
    """
    Get entries and bins from 1d histogram.
    """
    h = onedimhist
    x = h.GetXaxis()
    xsteps = (x.GetXmax()-x.GetXmin())/float(x.GetNbins())
    xmin = x.GetXmin()+xsteps/2.0
    xmax = x.GetXmax()+xsteps/2.0
    xmin -= xsteps/2.
    xmax -= xsteps/2.
    print 'xmin, xmax, xsteps:', xmin, xmax, xsteps
    xran = np.arange(xmin, xmax + xsteps, xsteps)
    z = []
    for x in xran:
        z.append(h.GetBinContent(h.FindBin(x)))
    return (xran, z)

def get_twodim_hist_entries(twodimhist):
    """
    Get entries and bins from 2d histogram.
    """
    #l = f.GetListOfKeys()
    #eff = l[0]
    #h = eff.ReadObj()
    #h.Draw()
    #print 'entries:', h.GetEntries()
    #print 'dimension:', h.GetDimension()
    h = twodimhist
    x = h.GetXaxis()
    xsteps = (x.GetXmax()-x.GetXmin())/float(x.GetNbins())
    xmin = x.GetXmin()+xsteps/2.0
    xmax = x.GetXmax()+xsteps/2.0
    #if xmin == 12.5:
    #    xmin -=12.5
    #    xmax -=12.5
    xmin -= xsteps/2.
    xmax -= xsteps/2.
    print 'xmin, xmax, xsteps:', xmin, xmax, xsteps
    xran = np.arange(xmin, xmax + xsteps, xsteps)
    y= h.GetYaxis()
    ysteps = (y.GetXmax()-y.GetXmin())/float(y.GetNbins())
    ymin = y.GetXmin()+ysteps/2.0
    ymax = y.GetXmax()+ysteps/2.0
    ymin -= ysteps/2.
    ymay -= ysteps/2.
    #if ymin == 12.5:
    #    ymin -=12.5
    #    ymax -=12.5
    print 'ymin, ymax, ysteps:', ymin, ymax + ysteps, ysteps
    yran = np.arange(ymin, ymax, ysteps)
    z = []
    for x in xran:
        for y in yran:
            z.append(h.GetBinContent(h.FindBin(x,y)))
    return (xran, yran, z)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print 'usage: python plots_danek.py [clus_ana.root]'
        exit(1)
    inputfile = sys.argv[1]
    file_description = inputfile[inputfile.index('clus_ana') + 8: inputfile.index('.root')].strip('_')

    # Get CMSSW area, at least CMSSW_9_2_0_patch2.
    # Get the code from git:
    #     git clone git@github.com:cms-analysis/DPGAnalysis-SiPixelTools.git DPGAnalysis-SiPixelTools
    # 
    #     Go to
    #     DPGAnalysis-SiPixelTools/HitAnalyzer/test
    #     and do:  scram b

    # See also 
    # ~dkotlins/public/CMSSW/CMSSW_9_2_0_patch2/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test

    # Scripts:
    # There are scripts to run individual  test:
    #     PixClusterAna.py - to look at all clusters
    #     testTracks.py - to look at clusters on track
    #     etc.
    #     For the present analysis I use: PixClusterAndTrack.py
    #     which combines all and on-track clusters.

    # cmsRun PixClusterAndTrack.py
    # gives clus_ana.root
    # clus_ana.root has 4 folders:
    #     (d) - all clusters
    #     (c) - clusters on tracks
    #     (c2) - clusters on tracks for tracks close to 0
    #     (c1) - clusters close to eta-1.8

    # (c) - clusters on tracks
    clusters_on_tracks = 'c'
    dirs = {'c': 'clusters on tracks', 'd': 'all clusters', 'c1': 'clusters on tracks for tracks close to \eta=0', 'c2': 'clusters close to \eta=1.8'}
    rootfile = rt.TFile(inputfile)

    # hEtaP - eta distribution of track which have at least 1 bpix hit
    for directory in dirs:
        if directory == 'd': continue
        track_eta_dist_geq_one_bpix_hit_overall = "hEtaP"
        obj = directory + '/' + track_eta_dist_geq_one_bpix_hit_overall
        print "retrieving " + str(obj) + "..."
        overall = rootfile.Get(obj)
        for layer in range(1, 5):
            # hEtaN & hPhiN - eta/phi distribution of tracks which have a hit in layer N
            track_eta_dist_geq_one_bpix_hit_lN = "hEta" + str(layer)

            # Get l1 efficiency in tracking:
            obj = directory + '/' + track_eta_dist_geq_one_bpix_hit_lN
            print "retrieving " + str(obj) + "..."
            lN = rootfile.Get(obj)
            lN.Draw()
            print "lNbins:", lN.GetNbinsX()
            # rt.gPad.SaveAs("track_eta_dist_geq_one_bpix_hit_lN.png")
            print "overall bins:", overall.GetNbinsX()
            overall.Rebin(overall.GetNbinsX()/lN.GetNbinsX())
            print "overall bins:", overall.GetNbinsX()

            xranlN, zlN = get_onedim_hist_entries(lN)
            xranoverall, zoverall = get_onedim_hist_entries(overall)
            xran = xranlN
            z = [zlN[i]/zoverall[i] if zoverall[i] != 0 else 0 for i in range(len(zoverall))]
            xsteps = xran[1] - xran[0]

            lN.Divide(overall)
            runs = file_description.replace('_', ' ').replace('N', str(layer))
            lN.SetTitle(runs + ' --- ' + dirs[directory])
            lN.Draw()

            fname = "track_eta_dist_geq_one_bpix_hit_eff_l" + str(layer) + "_" + directory + "_" +  file_description

            bins = []
            binedges = []
            for i in range(0,lN.GetNbinsX()+1):
                bins.append(lN.GetBinContent(i))
                #binedges.append(lN.GetBinEdges(i))
            #print bins
            #plt.hist(bins, label='paulsfabrication')
            print xran, z
            # z has overflow bin; bar takes only left edges
            plt.bar(xran[:-1], z[:-1], width=xsteps, label='ownfabrication')
            plt.legend()
            plt.savefig(fname + 'python.png')
            plt.close()

            rt.gPad.SaveAs(fname + ".png")
            rt.gPad.SaveAs(fname + ".pdf")
    exit(1)

    histos = {}
    histos['hpixPerDetN']    = "number of pixels per module in layer N"
    histos['hclusPerDetN']   = "number of clusters per module in layer N"
    histos['hchargeN']       = "cluster charge"
    histos['hpixchargeN']    = "pixel charge"
    histos['hsizeN']         = "cluster size"
    histos['hsizexN']        = "size in x direction (global rfi)"
    histos['hsizeyN']        = "size in y (global z)"
    histos['hpDetMapN']      = "2D map of number of pixels per module"
    histos['hDetMapN']       = "2D map of number of clusters per module"
    histos['hrocMapN']       = "pixels per ROC"
    # there are also many profile histograms versus bx (bunch crossing),
    # ls (lumi section), z (global z coordinate)...


    for histo in histos:
        for layer in range(1, 5):
            for directory in dirs:
                print directory
                obj = directory + '/' + histo[:-1] + str(layer)
                print "retrieving " + str(obj) + "..."
                hist = rootfile.Get(obj)
                #if type(hist) not in [rt.TH2F, rt.TH1F, rt.TH2D, rt.TH1D]:
                #    continue
                if type(hist) == rt.TObject: continue # Nullpointer!
                if hist.GetEntries() < 1: continue # Empty!
                hist.SetTitle(runs + ' --- ' + histos[histo] + ' in layer ' + str(layer) + ' --- ' + dirs[directory])
                if 'size' in histo:
                    print "adjusting size"
                    x = hist.GetXaxis()
                    x.SetLimits(0, 20)
                    x.SetRange(0, 20)

                hist.Draw()
                fname = histo[:-1] + str(layer) +  "_" + directory + '_' + file_description
                rt.gPad.SaveAs(fname + ".pdf")
                rt.gPad.SaveAs(fname + ".png")
                # add : eff vs bx
                #       cluster charge vs bx
                #       cluster size vs bx
                # Make note of different settings. then, compare same histos between runs!
                # run 29617[2,3,4] fill56750


        #canvas = rt.TCanvas()
        #if directory == 'd': continue






