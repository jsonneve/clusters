import ROOT as rt
inputfiles = ["clus_ana_298653_and_298678.root"]
inputfiles.append("clus_ana_297722_297723_vibiasbus100.root")
clusters_on_tracks = 'c'
track_eta_dist_geq_one_bpix_hit_l1 = "hEta1"
track_eta_dist_geq_one_bpix_hit_overall = "hEtaP"

inputfile = inputfiles[0]
rootfile = rt.TFile(inputfile)
obj = clusters_on_tracks + '/' + track_eta_dist_geq_one_bpix_hit_l1
l1 = rootfile.Get(obj)
l1.Draw()
rt.gPad.SaveAs("track_eta_dist_geq_one_bpix_hit_l1.png")
obj = clusters_on_tracks + '/' + track_eta_dist_geq_one_bpix_hit_overall
overall = rootfile.Get(obj)
overall.Rebin(overall.GetNbinsX()/l1.GetNbinsX())
l1.Divide(overall)
l1.Draw()

bins = []
for i in range(0,l1.GetNbinsX()+1):
    bins.append(l1.GetBinContent(i))
print bins
import matplotlib.pyplot as plt
plt.hist(bins)
plt.save('myhist.png')
rt.gPad.SaveAs("track_eta_dist_geq_one_bpix_hit_l1eff.png")





