import glob
import os
import sys



def write_and_replace(template, destination, replacements):
    r"""(str, str, dict) -> str
    Read out template and save to destination with
    the keys of replacements replaced by the corresponding
    values.
    >>> template = 'testing_temp.dat'
    >>> destination = 'testing_dest.dat'
    >>> fopen = open(template, 'w')
    >>> written = fopen.write('In this file\nI will replace\nsome words')
    >>> fopen.close()
    >>> replacements = {'replace': 'swap', 'words': 'letters', 'file': 'text'}
    >>> write_and_replace(template, destination, replacements)
    'testing_dest.dat'
    >>> fopen = open(destination, 'r')
    >>> fopen.read()
    'In this text\nI will swap\nsome letters'
    """
    source = open(template, 'r')
    dest = open(destination, 'w')
    line = source.readline()
    while line != '':
        for key in replacements:
            line = line.replace(key, str(replacements[key]))
        dest.write(line)
        line = source.readline()
    source.close()
    dest.close()
    return destination



if __name__ == "__main__":

    # Example
    # % ls /eos/cms/store/express/Run2017E/Express/FEVT/                  
    #Express-v1
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/Express/FEVT/Express-v1 
    #000
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/Express/FEVT/Express-v1/00
    #ls: cannot access /eos/cms/store/express/Run2017E/Express/FEVT/Express-v1/00: No such file or directory
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/Express/FEVT/Express-v1/000
    #303
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/Express/FEVT/Express-v1/000/304
    #ls: cannot access /eos/cms/store/express/Run2017E/Express/FEVT/Express-v1/000/304: No such file or directory
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017F                                
    #ls: cannot access /eos/cms/store/express/Run2017F: No such file or directory
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E
    #Express  ExpressCosmics  ExpressPhysics  HLTMonitor  StreamExpress  StreamExpressAlignment  StreamExpressCosmics  StreamHLTMonitor
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/ExpressCosmics 
    #FEVT
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/ExpressCosmics/FEVT 
    #Express-v1
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/ExpressCosmics/FEVT/Express-v1 
    #000
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/ExpressCosmics/FEVT/Express-v1/000 
    #303  304
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/ExpressCosmics/FEVT/Express-v1/000/304
    #002  009  015  020  028  045  055  058  060  074  093  115  129  134  150  152  155  165  182  188  192  201  205  212  226  244  248  259  282  290  300  303  312
    #003  012  018  023  032  054  057  059  061  086  104  117  131  143  151  154  160  168  186  191  194  203  206  213  237  245  249  269  286  299  301  311  313
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/ExpressCosmics/FEVT/Express-v1/000/304/226
    #00000
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test
    # % ls /eos/cms/store/express/Run2017E/ExpressCosmics/FEVT/Express-v1/000/304/192
    #00000
    #jsonneve@lxplus022 ~/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test


    # show files:
    if len(sys.argv) < 2:
        runnumber = 304226
    else:
        runnumber = sys.argv[1]
    searchpath = "/eos/cms/store/express/Run2017E/ExpressCosmics/FEVT/Express-v1/000/304/192"
    searchpath = "/eos/cms/store/express/Run2017*/Express*/FEVT/Express-v*/000/"
    searchpath = "/eos/cms/store/express/*201*/Express*/FEVT/Express-v*/000/"
    searchrunnumber = searchpath + str(runnumber)[:3] + "/" + str(runnumber)[3:] + "/*/*root"
    runfiles = glob.glob(searchrunnumber)
    print runfiles
    list_of_files = ''
    for runfile in runfiles:
        list_of_files = list_of_files + '"' + runfile.strip()[8:] + '",\n'
    print list_of_files
    cmsswdir = "/afs/cern.ch/user/j/jsonneve/public/CMSSW_9_2_9/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test/"
    cmsswdir = "/afs/cern.ch/user/j/jsonneve/public/CMSSW_10_0_3/src/DPGAnalysis-SiPixelTools/HitAnalyzer/test/"
    if 'cosmics' in runfiles[-1].lower():
        print "this was a cosmics run"
        template = "PixClusterAndTrack_template_cosmics.py"
    else:
        template = "PixClusterAndTrack_template.py"
    output = os.path.join(cmsswdir, template.replace("template", str(runnumber)))
    if os.path.exists(output):
        print "Output file already exists!", output
        sys.exit()
    write_and_replace(template, output, {"THIS_IS_YOUR_LIST_OF_FILES": list_of_files, 'clus_ana.root': 'clus_ana_' + str(runnumber) + '.root', 'RUNNUMBER': str(runnumber)})
    print "Done replacing, please find your file in", output
    print "You can do"
    print "cd " + cmsswdir
    print "source /afs/cern.ch/cms/cmsset_default.sh && export SCRAM_ARCH=slc6_amd64_gcc530"
    print "cmsenv"
    print "cmsRun " + output[output.rindex('/') + 1:]

    runnumber = 304192